# CI/CD Pipeline for static Azure website

The code in here is a CI/CD pipeline that deploys a simple website.

There are multiple stages and a staging environment and a production envrionment to simulate every step needed in a CI/CD pipeline.

Take a look around and feel free to ask me any questions about it!

Staging Environment - https://dfxgitlabteststaging.z13.web.core.windows.net/

Production Environemnt - https://dfxgitlabtest.z13.web.core.windows.net/
